void HTTP_init(void) {
  HTTP.on("/voltageAKB",handleVoltageAKB);    //http://192.168.0.101/voltageAKB
  HTTP.on("/powerSupply",handlePowerSupply);  //http://192.168.0.101/powerSupply
  HTTP.on("/powerSupplyState",handlePowerSupplyState); //http://192.168.0.101/powerSupplyState
  HTTP.on("/safe", handleActionSafe);                  //http://192.168.0.101/safe?action=  open/close
  HTTP.on("/safeState",handleSafeState);              //http://192.168.0.101/safeState
  HTTP.on("/reset", handleReset);           //http://192.168.0.101/reset?settings=ok
  HTTP.on("/reboot", handleReboot);         //http://192.168.0.101/reboot?device=ok
  httpUpdater.setup(&HTTP);
  HTTP.begin();
}

void handleVoltageAKB(){
  HTTP.send(200,"text/plain", String(voltageAKB));
}

void handlePowerSupply(){
  powerSupply();
  HTTP.send(200,"text/plain",powerSupplyState());
}

void handlePowerSupplyState(){
  HTTP.send(200,"text/plain",powerSupplyState());
}

void handleActionSafe(){   
  String action = HTTP.arg("action");
  if (action == "open")
  {
    openSafeWeb();
    HTTP.send(200,"text/plain",safeState());
  }
  if (action == "close")
  {
    closeSafe();
    HTTP.send(200,"text/plain", safeState());
  }
}

void handleSafeState(){
  HTTP.send(200,"text/plain",safeState());
}

void handleReset(){  
  String mes = HTTP.arg("settings");
  if (mes == "ok")
  {
  ESP.eraseConfig();
  ESP.reset();
  }
  HTTP.send(200, "text/plain", "OK");
}


void handleReboot() {
  String restart = HTTP.arg("device");
  if (restart == "ok") ESP.restart();
  HTTP.send(200, "text/plain", "OK");
}
