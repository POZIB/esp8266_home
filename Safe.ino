String closeSafe(){
 if (isStateSafe == 0)
 {
   timer = millis();
   while (millis() - timer <= 700){
     digitalWrite(pinCloseSafe,LOW);
   }
   isStateSafe = 1;
   digitalWrite(pinCloseSafe,HIGH);
   return String(isStateSafe);
 }
 else return String(isStateSafe);
}

String openSafeWeb(){
  timer = millis();
  while (millis() - timer <= 800){
    digitalWrite(pinOpenSafe,LOW);
  }
  isStateSafe = 0;
  digitalWrite(pinOpenSafe,HIGH);
  return String(isStateSafe);
}

String openSafeBtn(){
   digitalWrite(pinOpenSafe,LOW);
   isStateSafe = 0;
  return String(isStateSafe);
}

String safeState(){
  buttCloseSafe.tick();
  byte i = buttCloseSafe.isHold();
  if (i && isStateSafe == 1)  return "1";
  if (i)  return "1";
  return "0";
}
