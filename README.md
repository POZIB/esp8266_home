## Электронный замок на ESP8266

Электронный замок для сейфа на ESP8266(Wemos D1 Mini) c аналоговым способом открытия/закрытия и управлением через Web-интерфейс. 

#### Замок имеет:
- Web интерфейс управления сейфом;
- Аналоговое управление сейфом;
- Резервное питание на ~90ч;
- Режим энергосбережения(сна), если напряжение на резервном питании меньше 3v;
- Поднятие режим модема при настройки сети wifi;
- Сохранение и сброс настроек wifi;
- Автоматическое подключение к точке wifi, если ранее оно было создано;
- Обновление по "воздуху";
- Управление блоком питания через Web интерфейс;

Web интерфейс работает в локальной сети wifi, те возможности подключения в глобольной сети интернет ПОКА нет.

![](https://i0.wp.com/edistechlab.com/wp-content/uploads/2021/04/WeMos-d1-mini-Pin-out.png?ssl=1)