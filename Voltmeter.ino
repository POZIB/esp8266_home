int iteration = 0;
float midVoltage = 0;


float voltageCalculation(){
  float finalVoltage = 0;
  int valueAnalog = analogRead(0);
  midVoltage += 3.188*valueAnalog/1023*4.3;
  iteration++;
    if (iteration > 15){
      finalVoltage = midVoltage/iteration;
       iteration = 0;
       midVoltage = 0;
       return finalVoltage;
   }
}
