#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino
#include "GyverButton.h"
#include <ESP8266WebServer.h>
#include <WiFiManager.h>         //https://github.com/tzapu/WiFiManager
#include <ESP8266SSDP.h>
#include <ESP8266HTTPUpdateServer.h>
#include <FS.h>
//#include <aREST.h>

String  OTAUSER     =    "admin";    // Логин для входа в OTA
String  OTAPASSWORD =    "admin";    // Пароль для входа в ОТА
String  OTAPATH     =    "/firmware";// Путь, который будем дописывать после ip адреса в браузере.

GButton buttOpenSafe(13);
GButton buttCloseSafe(12);

byte isStateSafe = 0;
byte pinPS = 5;
byte pinOpenSafe = 4;
byte pinCloseSafe = 2;

unsigned long timer;
unsigned long timerUpdateVoltmeter;
unsigned long timerOpeningSafe;
unsigned long timer3;

float voltageAKB = 0;
bool isDischargedESP = false;
bool isStarting = false;

// Web интерфейс для устройства
ESP8266WebServer HTTP(80);
ESP8266HTTPUpdateServer httpUpdater;

// aREST и сервер для него
///WiFiServer SERVERaREST(8080);
//aREST rest = aREST();

File fsUploadFile;

void setup() {  
  pinMode(pinPS,OUTPUT); 
  pinMode(pinOpenSafe,OUTPUT);
  pinMode(pinCloseSafe,OUTPUT);
  digitalWrite(pinOpenSafe,HIGH);
  digitalWrite(pinCloseSafe,HIGH);
  
    // Настраиваем вывод отладки
  while(!Serial){}
  Serial.begin(115200);
  
    //Включаем WiFiManager
  WiFiManager wifiManager;
    //Если не удалось подключиться клиентом запускаем режим AP
    // доступ к настройкам по адресу http://192.168.4.1 
  wifiManager.setSTAStaticIPConfig(
    IPAddress(192,168,1,100), 
    IPAddress(192,168,1,1), 
    IPAddress(255,255,255,0)
  );  
  wifiManager.autoConnect("Safe","0123456789");
  
  httpUpdater.setup(&HTTP,OTAPATH, OTAUSER, OTAPASSWORD);
  
  Serial.println("Start FS инциализация");
  FS_init();
    //если подключение к точке доступа произошло сообщаем
  Serial.println("connected...yeey :)");
    //настраиваем HTTP интерфейс
  HTTP_init();
    //запускаем SSDP сервис 
  Serial.printf("Starting SSDP...\n");
  SSDP_init();
  Serial.printf("SSDP Ready!\n");

  voltageAKB = voltageCalculation();

  if (voltageAKB > 0 && voltageAKB < 2.8) {
    isDischargedESP = true;
    ESP.deepSleep(600e6); // засыпаем на 10 минут
  }
  if (voltageAKB >= 2.8 && voltageAKB < 3){
    isDischargedESP = true;
    ESP.deepSleep(300e6); // засыпаем на 5 минут
  }
}

void loop() {
  if (millis() - timerUpdateVoltmeter > 1000){
    timerUpdateVoltmeter = millis();
    voltageAKB = voltageCalculation();
  }
  
  if (voltageAKB >= 3 && isDischargedESP) {
    ESP.restart();
  } else if (voltageAKB < 3){
    isDischargedESP = true;
    ESP.deepSleep(30e6); // засыпаем на 30 сек
  }
  
  HTTP.handleClient();
  
  buttOpenSafe.tick();
  buttCloseSafe.tick();
  
  if (buttOpenSafe.isHold()){
    openSafeBtn();
    isStarting = true;
    timerOpeningSafe = millis();
  } else {
      digitalWrite(pinOpenSafe,HIGH); 
      
      if (buttCloseSafe.isHold() && !buttOpenSafe.isHold() && isStarting){
         if (millis() - timerOpeningSafe > 3000){
           closeSafe();
           timerOpeningSafe = millis();
         }
      }
  }
   
  
// put your main code here, to run repeatedly:
 // clientARest();
}
